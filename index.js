const { spawn } = require('child_process');
const _ = require('lodash')
const axios = require('axios')
const Hose = require('flowr-hose')

var hose = new Hose({
  name: 'jack-patchbay',
  port: process.env.PORT || 3000,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

hose.process('add', process.env.CONCURRENCY || 1, function(job, done){
  var found = false
  for (var i = 0; i < patchbay.length; i++) {
    if (patchbay[i].source == job.data.source && patchbay[i].target == job.data.target) {
      found = true
      break
    }
  }
  if(!found) {
    patchbay.push(job.data)
    processPatchbay(patchbay)
  }
  done()
})

hose.process('remove', process.env.CONCURRENCY || 1, function(job, done){
  var index = -1
  for (var i = 0; i < patchbay.length; i++) {
    if (patchbay[i].source == job.data.source && patchbay[i].target == job.data.target) {
      index = i
      break
    }
  }
  if(index!=-1) {
    patchbay.splice(index, 1)
  }
  done()
})

function processPatchbay (patchbay) {
  hose.create('jack-api:clients', {}, function (err, clients) {
    _.each(patchbay, function (v) {
      var source, target
      _.each(clients, function (ports, client) {
        if ((new RegExp(v.source.split(':')[0])).test(client)) {
          source = client + ':' + v.source.split(':')[1]
        }
        if ((new RegExp(v.target.split(':')[0])).test(client)) {
          target = client + ':' + v.target.split(':')[1]
        }
      })
      if (source && target) {
        var conns = clients[source.split(':')[0]].ports[source.split(':')[1]].connections
        var found = false
        for (var i = 0; i < conns.length; i++) {
          if (conns[i].client == target.split(':')[0] && conns[i].port == target.split(':')[1]) {
            found = true
            break
          }
        }
        if (!found) {
          hose.create('jack-api:connect', {
            source: source,
            target: target
          })
        }
      }
    })
  })
}

const command = spawn('stdbuf', ['-oL', '-eL', 'jack_evmon'])
const CLIENT_REGEX = /Client\ ([\w|-]*)\ (registered|unregistered)/
var patchbay = process.env.PATCHBAY ? require(process.env.PATCHBAY) : []

if (patchbay.length) processPatchbay(patchbay)

command.stdout.on('data', (data) => {
  var resultArray = CLIENT_REGEX.exec(data.toString())
  if(resultArray) {
    var client = resultArray[1]
    var action = resultArray[2]
    if (action === 'registered') {
      patchbay.map(function (v) {
        if ((new RegExp(v.source.split(':')[0])).test(client)) {
          processPatchbay(patchbay)
        } else if ((new RegExp(v.target.split(':')[0])).test(client)) {
          processPatchbay(patchbay)
        }
      })
    }
  }
});

command.stderr.on('data', (data) => {
  console.log(`stderr: ${data}`);
});

command.on('close', (code) => {
  console.log(`child process exited with code ${code}`);
});
